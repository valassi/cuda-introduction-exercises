Simpmle CUDA exercises, meant for complete beginners. 

Explore the GPU in your system by looking at its device properties.

Write your first own Hello World CUDA kernel and understand the hardware mapping of threads.

Work on vector addition and matrix multiplication examples to become more familiar with CUDA.

Follow the instructions in the pdf document and use the code snippets in the `exercises` folder to start from.
If you want to check your implementation, you can compare to the solution in the `solutions` folder, but only once you are done ;-)